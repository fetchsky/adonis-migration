"use strict";

const Helpers = use("Helpers");
const Cli = use("@adonisjs/ace");

class InitDataMigration extends Cli.Command {
  /**
   * Signature of the console command.
   *
   * @returns {String}
   */
  static get signature() {
    return "data-migration:init";
  }

  /**
   * Description of the console command.
   *
   * @returns {String}
   */
  static get description() {
    return "Setup essentials for this package";
  }

  /**
   * Execute the console command.
   *
   * @param {Object} args
   * @param {Object} options
   * @returns {Promise}
   */
  async handle(args, options) {
    await this.copy(__dirname + "/../../examples/migration.js", Helpers.migrationsPath("data_migration.js"));
    Cli.call("migration:run", {}, { silent: true });
  }
}

module.exports = InitDataMigration;
