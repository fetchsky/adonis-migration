"use strict";

const _ = use('lodash');
const Config = use('Config');
const moment = use("moment");
const Helpers = use("Helpers");
const Database = use("Database");
const { Command } = use("@adonisjs/ace");

class RunDataMigration extends Command {
  /**
   * Signature of the console command.
   *
   * @returns {String}
   */
  static get signature() {
    return "data-migration:run";
  }

  /**
   * Description of the console command.
   *
   * @returns {String}
   */
  static get description() {
    return "Run all pending data migrations";
  }

  static get migrationPath() {
    return Helpers.databasePath("data-migrations/");
  }

  /**
   * Execute the console command.
   *
   * @param {Object} args
   * @param {Object} options
   * @returns {Promise}
   */
  async handle(args, options) {
    try {
      const startTime = moment();
      this.migrationCount = 0;
      const files = await this.getFiles();
      const lastBatch = await Database.table("data_migrations").max("batch as batchNo");
      this.currentBatch = lastBatch[0].batchNo ? lastBatch[0].batchNo + 1 : 1;
      for (var file of files) {
        await this.migrateFile(file);
      }
      const endTime = moment();
      const totalTime = moment.duration(endTime.diff(startTime)).asMilliseconds() + " ms";
      if (this.migrationCount > 0) {
        this.success(`${this.icon("success")} Data migrated successfully in ${totalTime}`);
      } else {
        this.info(`Nothing to migrate`);
      }
      process.exit(0);
    } catch (err) {
      console.error(err);
      this.error(`${this.icon("error")} Data migrated failed`);
      process.exit(1);
    }
  }

  /**
   * This method is used get migration files
   */
  getFiles() {
    return new Promise(resolve => {
      const fs = require("fs");
      const files = fs.readdir(RunDataMigration.migrationPath, (err, files) => {
        if (err) {
          resolve([]);
        }
        resolve(files);
      });
    });
  }

  /**
   * This method is used to execute data migrations from file
   *
   * @param {String} fileName
   */
  async migrateFile(fileName) {
    const file = require(RunDataMigration.migrationPath + fileName);
    const ticketChunks = _.chunk(Object.keys(file), 50);
    try {
      await this.createTempTable();
      for (var tickets of ticketChunks) {
        const insertData = tickets.map(ticket => {
          return { name: ticket };
        });
        await Database.insert(insertData).into("temp_data_migrations");
      }
      const ticketToMigrate = await Database.table("temp_data_migrations")
        .whereNotIn("name", builder => {
          builder
            .select("name")
            .from("data_migrations")
            .where("file", fileName);
        })
        .pluck("name");
      if (ticketToMigrate.length) {
        for (var ticketName of ticketToMigrate) {
          await this.migrateTicket({
            fileName,
            queries: file[ticketName],
            ticket: ticketName
          });
        }
      }
    } finally {
      await this.dropTempTable();
    }
  }

  /**
   * This method is used to execute queries against specific ticket
   *
   * @param {Object} params
   * @param {String} params.ticket
   * @param {String} params.fileName
   * @param {Array} params.queries
   */
  async migrateTicket(params) {
    let dbConnections = {};
    let queries = [];
    try {
      for (let i = 0; i < params.queries.length; i++) {
        let query, connectionName;
        if (typeof params.queries[i] === 'string') {
          query = params.queries[i];
          connectionName = Config.get('database.connection');
        } else if (typeof _.get(params.queries[i], 'connection') === 'string' && typeof _.get(params.queries[i], 'query') === 'string') {
          query = params.queries[i].query;
          connectionName = params.queries[i].connection;
        }
        if (connectionName) {
          if (!dbConnections[connectionName]) {
            dbConnections[connectionName] = await Database.connection(connectionName).beginTransaction();
          }
          queries.push({
            trx: dbConnections[connectionName],
            query
          })
        }
      }
      for (let i = 0; i < queries.length; i++) {
        await queries[i].trx.raw(queries[i].query);
        this.migrationCount++;
      }
      for (let connectionName in dbConnections) {
        await dbConnections[connectionName].commit();
      }
      await Database.insert({
        name: params.ticket,
        file: params.fileName,
        batch: this.currentBatch
      }).into("data_migrations");
    } catch (err) {
      for (let connectionName in dbConnections) {
        await dbConnections[connectionName].rollback();
      }
      throw err;
    }
  }

  /**
   * This method is used to create temporary table
   */
  async createTempTable() {
    await Database.raw(`Create table if not exists temp_data_migrations (
      name VARCHAR(255),
      UNIQUE KEY temp_data_migrations_unique (name)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8`);
  }

  /**
   * This method is used to drop temporary table
   */
  async dropTempTable() {
    await Database.raw(`Drop table if exists temp_data_migrations`);
  }
}

module.exports = RunDataMigration;
