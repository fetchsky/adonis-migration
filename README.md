# adonis-migration

## Installation

`$ adonis install https://gitlab.com/fetchsky/adonis-data-migration.git --as adonis-migration`

## Registering provider & commands

Make sure to register this plugin's provider & commands. The providers and commands are registered inside `start/app.js` file.

```js
const providers = ["adonis-data-migration/providers/DataMigrationProvider"];
const commands = ["Adonis/Commands/DataMigration:Make", "Adonis/Commands/DataMigration:Run"];
```

## Generate data migration file

Execute the following command to generate the data migration file _database/data-migrations/v1.js_.
`$ adonis make:data-migration v1`

Add your INSERT/UPDATE/DELETE statements in _database/data-migrations/v1.js_.

## Run data migration

Execute the following command to run data migrations.
`$ adonis data-migration: run`
