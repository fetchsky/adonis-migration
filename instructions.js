"use strict";
const path = require("path");

module.exports = async function(cli) {
  try {
    let migrationFile = new Date().getTime() + "_data_migration.js";
    await cli.copy(path.join(__dirname, "./templates/migration.tmpl"), cli.helpers.migrationsPath(migrationFile));
    cli.command.completed("create", "database/migrations/" + migrationFile);
    cli.call("migration:run", {}, { silent: true });
  } catch (error) {
    // ignore errors
  }
};
