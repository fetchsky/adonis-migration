"use strict";

const { ServiceProvider } = use("@adonisjs/fold");

class DataMigrationProvider extends ServiceProvider {
  register() {
    this.app.bind("Adonis/Commands/DataMigration:Init", () => {
      return require("../src/Commands/Init");
    });
    this.app.bind("Adonis/Commands/DataMigration:Make", () => {
      return require("../src/Commands/Make");
    });
    this.app.bind("Adonis/Commands/DataMigration:Run", () => {
      return require("../src/Commands/Run");
    });
  }
}

module.exports = DataMigrationProvider;
