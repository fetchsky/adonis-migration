"use strict";

const Helpers = use("Helpers");
const { Command } = use("@adonisjs/ace");

class MakeDataMigration extends Command {
  /**
   * Signature of the console command.
   *
   * @returns {String}
   */
  static get signature() {
    return "make:data-migration {name: Name of file (e.g: v1.x)}";
  }

  /**
   * Description of the console command.
   *
   * @returns {String}
   */
  static get description() {
    return "Create a data migration file for INSERT/UPDATE/DELETE statements";
  }

  /**
   * Execute the console command.
   *
   * @param {Object} args
   * @param {Object} options
   * @returns {Promise}
   */
  async handle(args, options) {
    if (!args.name) {
      this.error("File name not specified");
      return false;
    }
    await this.copy(__dirname + "/../../templates/v1.x.tmpl", Helpers.databasePath("data-migrations/" + args.name + ".js"));
    this.success(`${this.icon("success")} created  database/data-migrations/${args.name}.js`);
  }
}

module.exports = MakeDataMigration;
